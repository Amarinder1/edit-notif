import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  //saveMessage: String;
  country = 'Argentina';
  target = 'Oil Fields';

  constructor() { }

  ngOnInit() {
  }

  // keyDownFunction(event) {
  //   if(event.keyCode == 13) {
  //     this.saveMessage = 'Saved!';
  //     document.getElementById('alert').style.display = 'block';
  //   }
  // }

}
